export const validateEmail = (input) => {
  var validRegex =
    /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  return input.match(validRegex);
};

export const validateString = (input) => {
  if (!input || input.trim().length <= 0) {
    return false;
  }
  return true;
};
