import axios from "axios";

class TodoService {
  todoApiUrl = "http://localhost:5001";
  async getTodos() {
    const uri = `${this.todoApiUrl}/v1/todos`;
    const res = await axios
      .get(uri, {})
      .then((res) => {
        return res.data;
      })
      .catch(function (e) {
        return [];
      });
    return res;
  }

  async deleteTodo(id) {
    const uri = `${this.todoApiUrl}/v1/todos/${id}`;
    const res = await axios
      .delete(uri, {})
      .then((res) => {
        return true;
      })
      .catch(function (e) {
        return false;
      });
    return res;
  }

  async updateTaskCompleteness({ id, isComplete }) {
    const uri = `${this.todoApiUrl}/v1/todos/${id}/isComplete`;
    const res = await axios
      .patch(uri, {
        isComplete: isComplete,
      })
      .then((res) => {
        return true;
      })
      .catch(function (e) {
        return false;
      });
    return res;
  }

  async createTodo(title) {
    const uri = `${this.todoApiUrl}/v1/todos`;
    const res = await axios
      .post(uri, {
        title: title,
        description: title,
        isComplete: false,
      })
      .then((res) => {
        return true;
      })
      .catch(function (e) {
        return false;
      });
    return res;
  }
}

const todoService = new TodoService();
export default todoService;
