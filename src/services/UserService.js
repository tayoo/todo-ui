import axios from "axios";

class UserService {
  userApiUrl = "http://localhost:5001";
  async getUserByEmail(email) {
    const uri = `${this.userApiUrl}/v1/users/${email}`;
    const res = await axios
      .get(uri, {})
      .then((res) => {
        return res.data;
      })
      .catch(function (e) {
        return [];
      });
    return res;
  }

  async createUser({ fullname, email, password }) {
    const uri = `${this.userApiUrl}/v1/users`;
    const res = await axios
      .post(uri, {
        fullname: fullname,
        email: email,
        password: password,
      })
      .then((res) => {
        return res.data.id;
      })
      .catch(function (e) {
        return false;
      });
    return res;
  }
}

const userService = new UserService();
export default userService;
