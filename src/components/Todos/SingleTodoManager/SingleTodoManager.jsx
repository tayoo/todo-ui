import React from "react";
import { Checkbox, Typography, Button, Grid } from "@mui/material";
import deleteIcon from "../../../assets/path-copy.svg";
import todoService from "../../../services/TodoService";

function SingleTodoManager(props) {
  const { todo, reloadParent, setActionInProgress } = props;

  const deleteTodo = async () => {
    setActionInProgress(true);
    await todoService.deleteTodo(todo.id);
    setActionInProgress(false);
    await reloadParent();
  };

  const updateTaskCompleteness = async () => {
    setActionInProgress(true);
    await todoService.updateTaskCompleteness({
      id: todo.id,
      isComplete: !todo.isComplete,
    });
    setActionInProgress(false);
    await reloadParent();
  };

  return (
    <Grid
      container
      key={todo.id}
      direction="row"
      display="flex"
      justifyContent="space-between"
      alignItems="center"
      fullWidth={true}
    >
      <Grid item xs={2}>
        <Checkbox
          onChange={() => {
            updateTaskCompleteness();
          }}
          checked={todo.isComplete}
        />
      </Grid>
      <Grid item xs={8}>
        <Typography variant="body1">{todo.title}</Typography>
      </Grid>
      <Grid item xs={2} display="flex" justifyContent="right">
        <Button onClick={deleteTodo}>
          <img src={deleteIcon} width="10px" height="15px" alt="logo" />
        </Button>
      </Grid>
    </Grid>
  );
}

export default SingleTodoManager;
