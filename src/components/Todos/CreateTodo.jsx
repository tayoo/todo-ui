import React, { useEffect, useState } from "react";
import { TextField } from "@mui/material";
import todoService from "../../services/TodoService";

function CreateTodo(props) {
  const { reloadParent, setActionInProgress } = props;
  const [newTodoTitle, setNewTodoTitle] = useState("");

  const createTodo = async () => {
    setActionInProgress(true);
    await todoService.createTodo(newTodoTitle);
    setActionInProgress(false);
    setNewTodoTitle("");
    reloadParent();
  };

  useEffect(() => {
    setNewTodoTitle("");
  }, []);

  const handleTitleChange = (event) => {
    setNewTodoTitle(event.target.value);
  };

  const handleKeyDown = (e) => {
    if (e.key === "Enter") {
      createTodo();
    }
  };

  return (
    <TextField
      label="Add a new todo"
      variant="standard"
      fullWidth={true}
      style={{ marginBottom: "20px" }}
      value={newTodoTitle}
      onChange={handleTitleChange}
      onKeyDown={handleKeyDown}
      onSubmit={createTodo}
    />
  );
}

export default CreateTodo;
