import React from "react";
import { Typography, Button } from "@mui/material";

function TodosFilter(props) {
  const { currentFilter, setCurrentFilter } = props;
  const updateFilter = (filterIndex) => {
    if (filterIndex !== currentFilter) setCurrentFilter(filterIndex);
  };
  const getLinkStyle = (filterIndex) => {
    let style = {
      marginLeft: "20px",
      textTransform: "none",
      textDecoration: "underline",
    };
    if (filterIndex === currentFilter) {
      style = {
        ...style,
        color: "black",
        textDecoration: "none",
      };
    }
    return style;
  };

  return (
    <Typography variant="body1" gutterBottom style={{ marginTop: "20px" }}>
      Show:
      <Button
        onClick={() => {
          updateFilter(0);
        }}
        style={getLinkStyle(0)}
      >
        All
      </Button>
      <Button
        onClick={() => {
          updateFilter(1);
        }}
        style={getLinkStyle(1)}
      >
        Completed
      </Button>
      <Button
        onClick={() => {
          updateFilter(2);
        }}
        style={getLinkStyle(2)}
      >
        Incompleted
      </Button>
    </Typography>
  );
}

export default TodosFilter;
