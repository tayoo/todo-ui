import React, { useEffect, useState } from "react";
import { Typography, LinearProgress, Button } from "@mui/material";
import logo from "../../assets/group.svg";
import SingleTodoManager from "./SingleTodoManager/SingleTodoManager";
import todoService from "../../services/TodoService";
import TodosFilter from "./TodosFilter";
import CreateTodo from "./CreateTodo";

function TodosManager(props) {
  const { setLoggedIn } = props;
  const [todos, setTodos] = useState([]);
  const [todosFilter, setTodosFilter] = useState(0);

  const [actionInProgress, setActionInProgress] = useState(false);

  const loadTodos = async () => {
    setActionInProgress(true);
    const todosList = await todoService.getTodos();
    setActionInProgress(false);
    setTodos(todosList);
  };

  useEffect(() => {
    loadTodos();
    setTodosFilter(0);
  }, []);

  const getFilteredTodos = () => {
    let filteredTodos;
    switch (todosFilter) {
      case 0:
        filteredTodos = todos;
        break;
      case 1:
        filteredTodos = todos.filter((todo) => todo.isComplete);
        break;
      default:
        filteredTodos = todos.filter((todo) => !todo.isComplete);
        break;
    }
    return filteredTodos;
  };

  return (
    <div class="Todos---Web">
      <Button
        style={{
          color: "black",
          textTransform: "none",
          position: "absolute",
          top: "12px",
          right: "15px",
        }}
        onClick={() => {
          setLoggedIn(false);
        }}
      >
        Logout
      </Button>
      <div class="Rectangle" style={{ margin: "40px" }}>
        <img src={logo} alt="logo" style={{ marginBottom: "20px" }} />
        <Typography variant="h6" gutterBottom>
          Todo List
        </Typography>
        {actionInProgress && <LinearProgress />}
        <CreateTodo
          reloadParent={loadTodos}
          setActionInProgress={setActionInProgress}
        />
        <div class="TodosList">
          {getFilteredTodos().map((todo) => {
            return (
              <SingleTodoManager
                todo={todo}
                reloadParent={loadTodos}
                setActionInProgress={setActionInProgress}
              />
            );
          })}
        </div>
        <TodosFilter
          currentFilter={todosFilter}
          setCurrentFilter={setTodosFilter}
        />
      </div>
    </div>
  );
}

export default TodosManager;
