import React, { useEffect, useState } from "react";
import { Typography, LinearProgress, TextField, Button } from "@mui/material";
import logo from "../../assets/group.svg";
import userService from "../../services/UserService";
import { validateEmail, validateString } from "../../utils/InputValidator";

function SignIn(props) {
  const { setNewUser, setLoggedIn } = props;
  const [actionInProgress, setActionInProgress] = useState(false);
  const [email, setEmail] = useState("");
  const [tried, setTried] = useState(false);
  const [password, setPassword] = useState("");
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    setActionInProgress(false);
    setTried(false);
    setEmail("");
    setPassword("");
  }, []);

  const checkCredentials = async () => {
    setTried(true);
    if (!validateEmail(email)) {
      setErrorMessage("Invalid email");
      setError(true);
      return;
    }
    if (!validateString(password)) {
      setErrorMessage("Invalid password");
      setError(true);
      return;
    }
    setActionInProgress(true);
    await userService
      .getUserByEmail(email)
      .then((user) => {
        if (user.password) {
          if (user.password === password) {
            setLoggedIn(true);
            setError(false);
            setEmail("");
          } else {
            setError(true);
            setErrorMessage("Invalid password");
          }
        } else {
          setError(true);
          setEmail("");
          setErrorMessage("This email doesn't exist.");
        }
      })
      .catch((error) => {
        setError(true);
        setErrorMessage("This email doesn't exist.");
        console.error(error);
        setEmail("");
      });
    setActionInProgress(false);
    setPassword("");
  };

  const handleInputChange = (event) => {
    switch (event.target.id) {
      case "email":
        setEmail(event.target.value);
        break;
      case "password":
        setPassword(event.target.value);
        break;
      default:
        break;
    }
  };

  return (
    <div class="Todos---Web">
      <div class="Rectangle" style={{ margin: "40px" }}>
        <img src={logo} alt="logo" style={{ marginBottom: "20px" }} />
        <Typography variant="h6" gutterBottom>
          Welcome back!
        </Typography>
        <Typography variant="body1" gutterBottom color="gray">
          Log in to continue
        </Typography>
        <TextField
          label="Email"
          variant="standard"
          fullWidth={true}
          id="email"
          value={email}
          onChange={handleInputChange}
          style={{ marginBottom: "20px" }}
          error={tried && !validateEmail(email)}
        />
        <TextField
          label="Password"
          variant="standard"
          fullWidth={true}
          id="password"
          value={password}
          onChange={handleInputChange}
          type="password"
          style={{ marginBottom: "20px" }}
          error={tried && !validateString(password)}
        />
        {error && (
          <Typography variant="caption" gutterBottom color="red">
            {errorMessage}
          </Typography>
        )}
        <br />
        {actionInProgress && <LinearProgress />}
        <Button
          style={{
            textTransform: "none",
            textDecoration: "underline",
            color: "black",
            textAlign: "left",
            marginBottom: "20px",
          }}
          onClick={() => {
            setNewUser(true);
          }}
        >
          Don't have an account? Sign up.
        </Button>
        <Button
          variant="contained"
          fullWidth={true}
          onClick={() => {
            checkCredentials();
          }}
          disabled={
            tried && (!validateEmail(email) || !validateString(password))
          }
        >
          Log In
        </Button>
      </div>
    </div>
  );
}

export default SignIn;
