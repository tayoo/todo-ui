import React, { useEffect, useState } from "react";
import { Typography, LinearProgress, TextField, Button } from "@mui/material";
import logo from "../../assets/group.svg";
import userService from "../../services/UserService";
import { validateEmail, validateString } from "../../utils/InputValidator";

function SignUp(props) {
  const { setNewUser } = props;
  const [fullname, setFullname] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [actionInProgress, setActionInProgress] = useState(false);
  const [tried, setTried] = useState(false);
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const createUser = async () => {
    setTried(true);
    if (!validateString(fullname)) {
      setErrorMessage("Invalid Full Name");
      setError(true);
      return;
    }
    if (!validateEmail(email)) {
      setErrorMessage("Invalid email");
      setError(true);
      return;
    }
    if (!validateString(password)) {
      setErrorMessage("Invalid password");
      setError(true);
      return;
    }
    setActionInProgress(true);
    await userService
      .createUser({
        fullname: fullname,
        email: email,
        password: password,
      })
      .then((id) => {
        if (id) setNewUser(false);
      });
    setActionInProgress(false);
    setFullname("");
    setEmail("");
    setPassword("");
  };

  const handleInputChange = (event) => {
    switch (event.target.id) {
      case "fullname":
        setFullname(event.target.value);
        break;
      case "email":
        setEmail(event.target.value);
        break;
      case "password":
        setPassword(event.target.value);
        break;
      default:
        break;
    }
  };

  useEffect(() => {
    setActionInProgress(false);
    setFullname("");
    setEmail("");
    setPassword("");
  }, []);

  return (
    <div class="Todos---Web">
      <div class="Rectangle" style={{ margin: "40px" }}>
        <img src={logo} alt="logo" style={{ marginBottom: "20px" }} />
        <Typography variant="h6" gutterBottom>
          Welcome!
        </Typography>
        <Typography variant="body1" gutterBottom color="gray">
          Sign up to start using Simpledo today.
        </Typography>
        <TextField
          label="Full Name"
          variant="standard"
          fullWidth={true}
          id="fullname"
          value={fullname}
          onChange={handleInputChange}
          style={{ marginBottom: "20px" }}
          error={tried && !validateString(fullname)}
        />
        <TextField
          label="Email"
          variant="standard"
          fullWidth={true}
          id="email"
          value={email}
          onChange={handleInputChange}
          style={{ marginBottom: "20px" }}
          error={tried && !validateEmail(email)}
        />
        <TextField
          label="Password"
          variant="standard"
          id="password"
          value={password}
          type="password"
          fullWidth={true}
          onChange={handleInputChange}
          style={{ marginBottom: "20px" }}
          error={tried && !validateString(password)}
        />
        {error && (
          <Typography variant="caption" gutterBottom color="red">
            {errorMessage}
          </Typography>
        )}
        <br />
        {actionInProgress && <LinearProgress />}
        <Button
          style={{
            textTransform: "none",
            textDecoration: "underline",
            color: "black",
            textAlign: "left",
            marginBottom: "20px",
          }}
          onClick={() => {
            setNewUser(false);
          }}
        >
          Do have an account? Sign in.
        </Button>
        <Button
          variant="contained"
          fullWidth={true}
          onClick={() => {
            createUser();
          }}
          disabled={
            tried && (!validateEmail(email) || !validateString(password))
          }
        >
          Sign up
        </Button>
      </div>
    </div>
  );
}

export default SignUp;
