import React, { useState } from "react";
import SignIn from "./SignIn";
import SignUp from "./SignUp";
import TodosManager from "../Todos/TodosManager";

function Welcome() {
  const [newUser, setNewUser] = useState(false);
  const [loggedIn, setLoggedIn] = useState(false);

  return (
    <div class="Todos---Web">
      {loggedIn ? (
        <TodosManager setLoggedIn={setLoggedIn} />
      ) : newUser ? (
        <SignUp setNewUser={setNewUser} />
      ) : (
        <SignIn setNewUser={setNewUser} setLoggedIn={setLoggedIn} />
      )}
    </div>
  );
}

export default Welcome;
